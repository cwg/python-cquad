% From p. 168 of Pedro Gonnet's thesis.

function y = f24(x)
    y = floor(exp(x));
end
