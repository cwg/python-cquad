% From p. 167 of Pedro Gonnet's thesis.

function y = fdiv(x)
    y = abs(x - 0.987654321).^-1.1;
end
